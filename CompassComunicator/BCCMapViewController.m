//
//  BCCMapViewController.m
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 23.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCMapViewController.h"
@interface BCCMapViewController()
@property BOOL _result;

@end
@implementation BCCMapViewController
@synthesize conn;
@synthesize _result;
- (IBAction)logout:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setConnector:(CGTConnector *)_conn{
    NSLog(@"Setting connector :%@",_conn);
    self.conn=_conn;
    [self.conn setDelegate:self];
}

-(void)didLoginWithResult:(BOOL)result{
    NSLog(@"RESULT %d ", result);
    self._result=result;
    if (!_result){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)viewDidLoad{
    _result=YES;
}
-(void)viewDidAppear:(BOOL)animated{
    if (!_result){
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}
@end
