//
//  CGTConnector.m
//  Compas
//
//  Created by Błażej Zyglarski on 24.08.2013.
//  Copyright (c) 2013 BEDESIGN. All rights reserved.
//

#import "CGTConnector.h"

@interface CGTConnector()
@property id<BCCConnDelegate> _delegate;
@end
@implementation CGTConnector
@synthesize conn;
@synthesize _delegate;
@synthesize _responseData;

-(void)startConnectionWithLogin:(NSString*)login andPassword:(NSString*)password{
    NSMutableURLRequest *request = nil;
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.compas.org.pl/?p_p_id=58&p_p_lifecycle=1&p_p_state=maximized&p_p_mode=view&saveLastPath=0&_58_struts_action=/login/login&_58_doActionAfterLogin=false&_58_login=%@&_58_password=%@",login,password]]];
    
    NSString *post = @"";	
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    [request setValue:[NSString stringWithFormat:@"%d", [postData length]] forHTTPHeaderField:@"Content-Length"];
    [request setTimeoutInterval: 15];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    _responseData = [[NSMutableString alloc] initWithString:@""];
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [conn start];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding	] ;
    [_responseData appendString:responseString];

    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"###%@",error);
    NSLog(@"###%@",_responseData);
    
    if ([[_responseData componentsSeparatedByString:@"chatPokazZnajomych(this)"] count]>1){
        [_delegate didLoginWithResult:YES];
    }else{
        [_delegate didLoginWithResult:NO];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    BOOL isOK = [[_responseData componentsSeparatedByString:@"chat_pozwol_na_mape"] count]>1;
    
    [self._delegate didLoginWithResult:isOK];
}
-(void)checkInfo{
}
-(void) setDelegate:(id <BCCConnDelegate>)del{
    self._delegate=del;
}


-(void)clearCookies{
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *each in cookieStorage.cookies) {
        [cookieStorage deleteCookie:each];
    }

}
-(BOOL)sendData:(NSMutableDictionary*)data to:(NSString*)target target:(id)t selector:(SEL)s{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLog(@"LANG: %@",language);
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.compas.org.pl/%@/%@",language,target]];
    
    NSArray *cookieStorage = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:url];
    NSDictionary *cookieHeaders = [NSHTTPCookie requestHeaderFieldsWithCookies:cookieStorage];
    for (NSString *key in cookieHeaders) {
        NSString* elements = cookieHeaders[key];
        NSArray * a = [elements componentsSeparatedByString:@"; "];
        for(NSString * c in a){
            NSArray * b = [c componentsSeparatedByString:@"="];
            if([b[0] isEqualToString:@"JSESSIONID"]){
                NSString * key = @"JSESSIONID";
                if (data!=nil)
                    [data setObject:b[1] forKey:b[0]];
                    //NSLog(@"Cookie: %@ = %@", b[0],b[1]);
            }
            
        }
    }
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    NSString * post=  [NSString stringWithFormat:@"timest=%@",timeStampObj];
    for(NSString * key in data.allKeys){
        post = [NSString stringWithFormat:@"%@&%@=%@",post,key,data[key] ];
    }
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
	
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
	
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody:postData];
	[request setCachePolicy:NSURLCacheStorageNotAllowed];
    
    NSURLResponse *response;
    NSError *err;
     [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         //NSLog(@"receiving daata len %d", [data length]);
         [t performSelector:s withObject:newStr];
     }];
    
    
   
    return TRUE;
}

-(NSString* )stripDataInsideTag:(NSString*)tag from:(NSString*)resp{
    if(resp){
        NSArray* a =[resp componentsSeparatedByString:[NSString stringWithFormat:@"<%@>",tag]];
        if(a){
            NSArray* b =[a[1] componentsSeparatedByString:[NSString stringWithFormat:@"</%@>",tag]];
            if(b){
                return b[0];
            }
        }
    }
    return nil;
}

-(NSString* )findLastMessageOfType:(NSString*)type in:(NSString*)resp{
    NSString * result=nil;
    NSArray * a = [resp componentsSeparatedByString:@"portlet-msg-"];
    for(int i = 1;i<a.count;i++){
        NSArray * b = [a[i] componentsSeparatedByString:@"</div"];
        
        NSArray * d = [b[0] componentsSeparatedByString:@"\">"];
        
        if ([type isEqualToString:d[0]]){
            result = d[1];
        }
    }

    return result;
}

@end
