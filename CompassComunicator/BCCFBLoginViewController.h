//
//  BCCFBLoginViewController.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 15.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCCFBLoginViewController : UIViewController<UIWebViewDelegate>

@end
