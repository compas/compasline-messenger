//
//  BCCViewController.m
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 15.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCViewController.h"
#import "BCCMapViewController.h"
@interface BCCViewController ()
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIView *MainView;

@end

@implementation BCCViewController
@synthesize conn;
@synthesize password;
@synthesize email;
- (void)viewDidLoad
{
    [super viewDidLoad];
    conn = [[CGTConnector alloc]init];
	[conn setDelegate: self];
    
    
    
}
- (IBAction)Login:(id)sender {
    [conn startConnectionWithLogin:email.text andPassword:password.text];
}
- (IBAction)StartEditing:(id)sender {
    [UIView animateWithDuration:1.0 animations:^{
        self.MainView.center=CGPointMake(160, self.view.center.y-150);
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    password.text= [[NSUserDefaults standardUserDefaults] objectForKey:@"_58_password"];
    email.text= [[NSUserDefaults standardUserDefaults] objectForKey:@"_58_emailAddress"];
    [conn clearCookies];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqual:@"login"]){
        BCCMapViewController* bv  =(BCCMapViewController*) [segue destinationViewController];
        [bv setConnector:conn];
        [conn startConnectionWithLogin:email.text andPassword:password.text];
    }
}
-(void)didLoginWithResult:(BOOL) result{
    if (result){
        [self performSegueWithIdentifier:@"loggedin" sender:self];
    }
}


@end
