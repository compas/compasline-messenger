//
//  BCCViewController.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 15.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CGTConnector.h"
#import "BCCConnDelegate.h"

@interface BCCViewController : UIViewController<BCCConnDelegate>
@property CGTConnector* conn;
@end
