//
//  BCCConnDelegate.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 23.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BCCConnDelegate <NSObject>

-(void)didLoginWithResult:(BOOL) result;

@end
