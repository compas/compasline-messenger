//
//  BCCFBLoginViewController.m
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 15.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCFBLoginViewController.h"

@interface BCCFBLoginViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *FBLoginWebView;
@property NSInteger count;

@end

@implementation BCCFBLoginViewController
@synthesize count;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    count=0;
    
    self.FBLoginWebView.delegate=self;
    [self.FBLoginWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://graph.facebook.com/oauth/authorize?client_id=292136260893314&redirect_uri=http%3A%2F%2Fwww.compas.org.pl%2Fc%2Flogin%2Ffacebook_connect_oauth%3Fredirect%3Dhttp%25253A%25252F%25252Fwww.compas.org.pl%25252Fhome%25253Fp_p_id%25253D58%252526p_p_lifecycle%25253D0%252526p_p_state%25253Dpop_up%252526p_p_mode%25253Dview%252526_58_struts_action%25253D%2525252Flogin%2525252Flogin_redirect&scope=email"]]];
    // Do any additional setup after loading the view.
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"%@",request.URL.host);
    
    if ([request.URL.host isEqualToString:@"www.compas.org.pl"]){
        count +=1;
        self.FBLoginWebView.alpha=0.0;
    }
    
    
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    if(count>=2){
        [self performSegueWithIdentifier:@"FBLoginSuccess" sender:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
