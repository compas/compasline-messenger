//
//  BCCAppDelegate.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 15.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
