//
//  BCCMapViewController.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 23.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CGTConnector.h"
#import "BCCConnDelegate.h"

@interface BCCMapViewController : UIViewController <BCCConnDelegate>
@property CGTConnector* conn;
-(void)setConnector:(CGTConnector *)_conn;
@end
