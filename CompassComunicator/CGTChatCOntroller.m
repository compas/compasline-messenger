//
//  CGTChatCOntroller.m
//  Compas
//
//  Created by Błażej Zyglarski on 02.10.2013.
//  Copyright (c) 2013 BEDESIGN. All rights reserved.
//

#import "CGTChatCOntroller.h"

@implementation CGTChatCOntroller
@synthesize standardUserDefaults;
@synthesize userid;
@synthesize windows;
@synthesize timestamp;
@synthesize vi;

-(id)init{
    self = [super init];
    if (self){
        standardUserDefaults = [NSUserDefaults standardUserDefaults];
        self.windows= [NSMutableDictionary dictionaryWithCapacity:1];
        if(standardUserDefaults){
           self.userid = [standardUserDefaults valueForKey:@"_58_emailAddress"];
           NSLog(@"Userid: %@",userid);
            
        }
    }
    self.timestamp=0;
    return self;
}


-(void)getMessage:(NSDictionary*)wiadomosc{
    
    NSLog(@"Opening: %@",userid);
    NSString * u_nadawca = [wiadomosc valueForKey:@"u_nadawca"];
    NSString * format = @"http://www.compas.org.pl/Chat/chatengine.jsp?op=otwarte_okienko&nadawca=%@&odbiorca=%@";
    NSString *ursl = [NSString stringWithFormat:format,userid,u_nadawca ];
    
    NSLog(@"OTWIERANIE: %@",ursl);
    
    
}

-(void)check{
 //   NSLog(@"checkng: %@",userid);
    NSString * format = @"http://www.compas.org.pl/Chat/chatengine.jsp?op=info&user=%@&ts=%@";
    NSString *ursl = [NSString stringWithFormat:format,userid, timestamp];
    NSData *apiResponse = [NSData dataWithContentsOfURL:[NSURL URLWithString:ursl ]];
    NSDictionary * response = [NSJSONSerialization JSONObjectWithData:apiResponse options:nil error:nil];
    self.timestamp = [NSNumber numberWithInteger:[[response valueForKey:@"ts"] integerValue]];
   // NSLog(@"resp: %@",response);
    
    for (NSDictionary* wiadomosc in [response valueForKey:@"wiadomosci"]){
        
        [self getMessage:wiadomosc];
        
    }
}
@end
