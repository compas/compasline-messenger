//
//  UILabel+HtmlLabel.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 11.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HtmlLabel)
- (void) setHtml: (NSString*) html;

@end
