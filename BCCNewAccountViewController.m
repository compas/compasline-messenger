//
//  BCCNewAccountViewController.m
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 17.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCNewAccountViewController.h"
#import "CGTConnector.h"
#import "UILabel+HtmlLabel.h"

@interface BCCNewAccountViewController()
@property NSString* pAuth;
@property CGTConnector* c ;
@property NSString* _58_screenName;
@property NSString* _58_password;
@property NSString* _58_emailAddress;
@property BOOL created;

@end
@implementation BCCNewAccountViewController

@synthesize _58_screenName;
@synthesize _58_password;
@synthesize _58_emailAddress;

@synthesize pAuth;
@synthesize c;
@synthesize created;

-(void)viewWillAppear:(BOOL)animated{
    [self.ErrorLabel setAlpha:0];
    [self.ErrorLabel setBackgroundColor:[UIColor redColor]];
    self.created=FALSE;
}
-(void)viewDidLoad{
    c= [[CGTConnector alloc]init];
    //[NSHTTPCookieStorage sharedHTTPCookieStorage]
    
    [c clearCookies];
    [c sendData:nil to:@"home?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&saveLastPath=0&_58_struts_action=%2Flogin%2Fcreate_account" target: self selector:@selector(pAuthResponse:)];
    }
- (IBAction)startEditing:(id)sender {
    [UIView animateWithDuration:1.0 animations:^{
        self.wholeview.center=CGPointMake(160, self.view.center.y-150);
    }];
}

-(void)pAuthResponse:(id)resp{
      
}

- (IBAction)createAccount:(id)sender {

    if(self.created){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
    NSString * _58_firstName =@"";
    NSString * _58_lastName =@"";
    _58_screenName =(NSString*)[self.email.text componentsSeparatedByString:@"@"].firstObject;
    _58_emailAddress =self.email.text;
    
    
    
    NSArray*e = [self.Name.text componentsSeparatedByString:@" "];
    if([e count]>1){
        _58_firstName = (NSString * )e.firstObject;
        _58_lastName = (NSString * )e.lastObject;
    }else{
        _58_firstName=self.Name.text;
    }
    
    
    NSDictionary *parameters = @{@"_58_firstName": _58_firstName,
                                 @"_58_lastName": _58_lastName,
                                 @"_58_screenName": _58_screenName,
                                 @"_58_emailAddress": _58_emailAddress,
                                 @"_58_cmd":@"add",
                                 @"_58_birthdayDay":@"1",
                                 @"_58_birthdayMonth":@"1",
                                 @"_58_birthdayYear":@"1970",
                                 @"_58_male":@"1",
                                 @"_58_redirect":@"",
                                 @"_58_openId":@"",
                                 @"_58_formDate":@"1399830098690",
                                 @"p_p_id":@"5",
                                 @"p_p_lifecycle":@"1",
                                 @"p_p_state":@"maximized",
                                 @"p_p_mode":@"view",
                                 @"saveLastPath":@"0",
                                 @"_58_struts_action":@"/login/create_account"};
    
    NSMutableDictionary *parameters2 = [[NSMutableDictionary alloc]initWithDictionary:parameters];
    
    [c sendData:parameters2 to:
     [NSString stringWithFormat:@"home?p_p_id=58&p_p_lifecycle=1&p_p_state=maximized&p_p_mode=view&saveLastPath=0&_58_struts_action=%@",@"%2Flogin%2Fcreate_account"] target:self selector:@selector(createAccountResult:)
     ];
    }
}
-(void)createAccountResult:(id)resp{
    @try {
        NSString * info = [c findLastMessageOfType:@"success" in:resp];
        if (info){
            //UDAŁO SIĘ
            NSLog(@"Założono konto: %@", info);
            _58_password = [c stripDataInsideTag:@"strong" from:info];
            NSLog(@"Założono konto: %@, %@",_58_screenName, _58_password);
            
            [[NSUserDefaults standardUserDefaults] setObject:_58_password forKey:@"_58_password"];
            [[NSUserDefaults standardUserDefaults] setObject:_58_screenName forKey:@"_58_screenName"];
            [[NSUserDefaults standardUserDefaults] setObject:_58_emailAddress forKey:@"_58_emailAddress"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            self.CNAButton.titleLabel.text=@"OK, return to login";
            self.created=TRUE;
            [UIView animateWithDuration:0.5 animations:^{
                [self.ErrorLabel setAlpha:0.0];
            } completion:^(BOOL b){
                
                [self.ErrorLabel setHtml:info];
                [self.ErrorLabel setBackgroundColor:[UIColor colorWithRed:0. green:0.5 blue:0. alpha:1.]];
                [UIView animateWithDuration:0.5 animations:^{
                    [self.ErrorLabel setAlpha:1.0];
                }];
    
            }];
        }else{
            NSString * error = [c findLastMessageOfType:@"error" in:resp];
            if(error){
                self.ErrorLabel.text= error;
                [UIView animateWithDuration:1.0 animations:^{
                    [self.ErrorLabel setAlpha:1.0];
                }];

            }
        }
       // NSLog(@">>>>>>>>");
       // N/SLog(@"%@",resp);
       // NSLog(@"<<<<<<<<");
        
    }
    @catch (NSException *exception) {
        NSLog(@"E:%@",exception);
        //NSLog(@"R:%@",resp);
        
    }
    @finally {
    }
    
        
    
}
@end
